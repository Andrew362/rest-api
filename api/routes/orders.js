const express = require('express');
const router = express.Router();


router.get('/', (req, res, next) => {
    res.status(200).json({
        message: 'orders get'
    });
});

router.post('/', (req, res, next) => {
    res.status(201).json({
        message: 'orders post'
    });
});

router.get('/:orderId', (req, res, next) => {
    res.status(200).json({
        message: 'orders detail',
        orderID: req.params.orderID
    });
});

router.delete('/:orderId', (req, res, next) => {
    res.status(200).json({
        message: 'orders deleted',
        orderID: req.params.orderID
    });
});

module.exports = router;
